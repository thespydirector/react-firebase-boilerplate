# React+Firebase Boilerplate

Useful starting point for react/firebase projects.

## What to do

** Go to firebase console and create new project.
** Add every service you need to add.
** Initialize firebase project on cmd with firebase init.
** To add firebase to code, uncomment all firebase syntax.
** Replace creds in firebase context.
** Code away.
