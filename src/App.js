import './styles/App.css';
import Main from './components/main.jsx';
/* import FirebaseContextProvider from './contexts/firebase.jsx'; */
import {BrowserRouter} from 'react-router-dom';

function App() {

  //const fire = useContext(FirebaseContext);

  return (
      <BrowserRouter>
          <Main/>
      </BrowserRouter>
    );
}

export default App;


/**
 * With firebase
 * 
 *   return (
    <FirebaseContextProvider>
      <BrowserRouter>
          <Main/>
      </BrowserRouter>
    </FirebaseContextProvider>
  );
 * 
 */