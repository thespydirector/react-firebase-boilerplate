import { useContext, useEffect, useState } from "react";
import {Link} from 'react-router-dom';
import logo from '../assets/logo.svg';
//import { FirebaseContext } from "./contexts/firebase.jsx";
import '../styles/App.css';


function Landing() {

    //const fire = useContext(FirebaseContext);
  
    return (
        <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
  
  export default Landing;