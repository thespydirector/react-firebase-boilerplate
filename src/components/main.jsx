import React, {useEffect,useContext, useState} from 'react';
import {Switch,Route,Redirect} from 'react-router-dom';
//import { FirebaseContext } from '../contexts/firebase.jsx';
import Landing from './landing.jsx';

function Main(){
    //const fire = useContext(FirebaseContext);

/*     useEffect(()=>{
        fire.watch();
    },[fire]) */
    
    return(
        <Switch>
            <Route exact path="/" component={Landing}/>
        </Switch>
    )
}

export default Main;